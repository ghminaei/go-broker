package main

import (
    "fmt"
    "log"
    "net"
    "encoding/gob"
    "os"
    "bufio"
)

const NORMAL_MSG = "MSG";
const ACK_MSG = "ACK";
const OVFERFLOW_MSG = "OVF";
const PUB_MSG = "PUB";

const SYNC = "sync";
const ASYNC = "async";

const DEFULT_PORT = ":8080";

const QUEUE_SIZE = 1024;

type Config struct {
    Port    string;
    SType   string;
}

type Message struct {
	Number int;
	Data   string;
	MType  string;
}


func isSync(arg string) string {
    if arg == "-s" {
        return SYNC;
    } else if arg == "-a" {
        return ASYNC
    }
    return ""
}

func setConfig(config *Config) {
    arguments := os.Args
	if len(arguments) == 1 {
        fmt.Println("I use default setting: async.");
        fmt.Println("and default ip and port number: localhost:8080.");
        config.Port = DEFULT_PORT
        config.SType = ASYNC;

	} else if len(arguments) == 2 {
        res := isSync(arguments[1]);
        if res == "" {
            config.Port = arguments[1];
        } else {
            config.Port = DEFULT_PORT
            config.SType = res;
        }

    } else if len(arguments) == 3 {
        if isSync(arguments[2]) == "" {
            config.SType = ASYNC;
        } else {
            config.SType = isSync(arguments[2]);
        }
        config.Port = arguments[1];

    } 
}

func receiveAck(conn net.Conn, acks chan *Message, config *Config) {
    decoder := gob.NewDecoder(conn)
    for {
        m := &Message{};
        err := decoder.Decode(m);
        
        if (err != nil) {
			fmt.Println("broker is not alive.")
			conn.Close()
			return
        }
        if (config.SType == ASYNC) {
            fmt.Println("acked: ", m.Number)
        }
        
        if m.MType == OVFERFLOW_MSG {
            fmt.Println("Broker buffer is full, we are storing data here")
        }
        if (config.SType == SYNC) {
            acks <- m;
        }
    }
}

func sendData(conn net.Conn, buffer chan *Message, acks chan *Message, config *Config) {
    encoder := gob.NewEncoder(conn);
    for {
        p := <- buffer;
        fmt.Println("sending...")
        encoder.Encode(p)

        if (config.SType == SYNC) {
            fmt.Println("waiting for ack:")
            ack := <- acks;
            fmt.Println("acked: ", ack.Number)
        }
    }
}

func sendInitalMsg(conn net.Conn) {
    fmt.Println("Channel name:" )
    var name string;
    fmt.Scanln(&name);
    p := &Message{}
    p.Number = -1;
    p.Data = name;
    p.MType = PUB_MSG;
    encoder := gob.NewEncoder(conn);
    encoder.Encode(p)
}

func main() {
    fmt.Println("Start Server");
    var config Config;

    buffer := make(chan *Message, QUEUE_SIZE);
    setConfig(&config);
    conn, err := net.Dial("tcp", config.Port)
    if err != nil {
        log.Fatal("Connection error", err)
    }

    sendInitalMsg(conn);

	acks := make(chan *Message);

    go receiveAck(conn, acks, &config);
    go sendData(conn, buffer, acks, &config);

    counter := 0;

    for {
        fmt.Println("Type your message: ")
        p := &Message{}
        var msg string ;
        reader := bufio.NewReader(os.Stdin)
	    msg, err := reader.ReadString('\n')
	    if err != nil {
		    panic(err)
	    }

        p.Number = counter;
        p.Data = msg;
        p.MType = NORMAL_MSG;
        counter += 1;
        if counter == QUEUE_SIZE {
            counter = 0;
        } 
        if len(buffer) == QUEUE_SIZE {
            fmt.Println("Buffer is full! you should wait...")
        }
        buffer <- p;
    }
}
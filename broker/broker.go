package main

import (
    "fmt"
    "net"
	"encoding/gob"
	"os"
	"io"
)

var isServerAlive bool;
const OVFERFLOW_MSG = "OVF";
const PUB_MSG = "PUB";
const SUB_MSG = "SUB";
const DEFULT_PORT = ":8080";
const QUEUE_SIZE = 1024;

type Message struct {
	Number int;
	Data   string;
	MType  string;
}


func receiveFromServer(conn net.Conn, queue chan *Message) {
	decoder := gob.NewDecoder(conn)
    for {
		m := &Message{}
		fmt.Println("ready to receive from server:");
		err := decoder.Decode(m)
		if (err != nil) {
			fmt.Println("server is not alive.")
			isServerAlive = false;
			conn.Close()
			return
		}
		queue <- m;
        fmt.Println("New message from server: ");
        fmt.Printf("Received num : %d \n", m.Number);
        fmt.Printf("Received data: %s \n", m.Data);
		fmt.Printf("Received type: %s \n", m.MType);
		
	}
}


func sendDataToClient(conn net.Conn, queue chan *Message, acks chan *Message) {
	encoder := gob.NewEncoder(conn)
	for {
		m := <- queue;
		encoder.Encode(m)
		res := receiveAckFromClient(conn, acks)
		if res == "EOF" {
			fmt.Println("Client is not alive")
			conn.Close()
			queue <- m; //reQueue data
			return
		}
	}
}


func sendAckToServer(conn net.Conn, acks chan *Message) {
	encoder := gob.NewEncoder(conn)
	for {
		if (isServerAlive) {
			m := <- acks;
			encoder.Encode(m)
			fmt.Printf("send ack to server");
		} else {
			return
		}
	}
}

func receiveAckFromClient(conn net.Conn, acks chan *Message) string {
	decoder := gob.NewDecoder(conn)
	m := &Message{}
	err := decoder.Decode(m)
	if (err == io.EOF) {
		return "EOF"
	}

	fmt.Printf("Received from client num : %d \n", m.Number);
	fmt.Printf("Received from client data: %s \n", m.Data);
	fmt.Printf("Received from client type: %s \n", m.MType);
	if (m.MType == "ACK") {
		if (isServerAlive) {
			acks <- m;
		}
	} else {
		fmt.Printf("packet is not ACK");
	}
	return "OK"
}

func monitorBufferOverflow(conn net.Conn, queue chan *Message, acks chan *Message) {
	isSent := false
	for {
		if (len(queue) == QUEUE_SIZE) && isSent == false {
			p := &Message{}
        	p.Number = -1;
        	p.Data = "";
			p.MType = OVFERFLOW_MSG;
			acks <- p;
			isSent = true
		}
		if (len(queue) < QUEUE_SIZE) && isSent == true {
			isSent = false;
		}
	}
}

func getPort() string {
	arguments := os.Args
	if len(arguments) == 1 {
		fmt.Println("I use default ip and port number: localhost:8080.");
		return DEFULT_PORT;
	}
	return arguments[1];
	
}


func handleNewConnection(conn net.Conn, queues (map [string] (chan *Message)), acks chan *Message) {
	decoder := gob.NewDecoder(conn)
	m := &Message{}
	err := decoder.Decode(m)
	if (err != nil) {
		fmt.Println("connection is not alive.")
		conn.Close()
		return
	}
	fmt.Println("New message from New Connection: ");
	fmt.Printf("Received num : %d \n", m.Number);
	fmt.Printf("Received data: %s \n", m.Data);
	fmt.Printf("Received type: %s \n", m.MType);

	if m.MType == PUB_MSG && isServerAlive == false {
		isServerAlive = true
		queues[m.Data] = make(chan *Message, QUEUE_SIZE);
		go receiveFromServer(conn, queues[m.Data]);
		go sendAckToServer(conn, acks);
		go monitorBufferOverflow(conn, queues[m.Data], acks);
	} else if m.MType == SUB_MSG {
		if queue, ok := queues[m.Data]; ok {
			go sendDataToClient(conn, queue, acks);
		} else {
			fmt.Printf("no channel named: %s\n", m.Data);
		}
	} else {
		fmt.Println("not valid request");
		conn.Close()
		return
	}
}

func main() {
	isServerAlive = false;
	port := getPort();
	queues := make(map[string] chan *Message)
	acks := make(chan *Message, QUEUE_SIZE+2);

	fmt.Println("Broker started...");
	
	ln, err := net.Listen("tcp", port);
	if err != nil {
		fmt.Println("Can't bind to socket!");
		return
	}

	for {
        conn, err := ln.Accept();
        if err != nil {
            fmt.Println("Couldn't connect to client!");
            continue;
		}
		
		fmt.Printf("New Connection %s\n", conn.RemoteAddr().String())
		go handleNewConnection(conn, queues, acks);
	}
}
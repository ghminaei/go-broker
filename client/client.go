package main

import (
    "fmt"
    "log"
    "net"
    "encoding/gob"
    "os"
)

const DEFULT_PORT = "localhost:8080";
const ACK_MSG = "ACK";
const SUB_MSG = "SUB";



type Message struct {
	Number int;
	Data   string;
	MType  string;
}

func getPort() string {
	arguments := os.Args
	if len(arguments) == 1 {
		fmt.Println("I use default ip and port number: localhost:8080.");
		return DEFULT_PORT;
	}
	return arguments[1];
	
}


func sendInitalMsg(conn net.Conn) {
    fmt.Println("Channel name:" )
    var name string;
    fmt.Scanln(&name);
    p := &Message{}
    p.Number = 0;
    p.Data = name;
    p.MType = SUB_MSG;
    encoder := gob.NewEncoder(conn);
    encoder.Encode(p)
}


func main() {
    fmt.Println("start client");
    conn, err := net.Dial("tcp", getPort())
    if err != nil {
        log.Fatal("Connection error", err)
    }
    
    sendInitalMsg(conn);

	decoder := gob.NewDecoder(conn)
    for {
        m := &Message{}
        err := decoder.Decode(m)
        if (err != nil) {
            fmt.Println("broker is not alive.")
            fmt.Println(err);
			break;
		}
        fmt.Println("New message from broker: ");
        fmt.Printf("Received num : %d \n", m.Number);
        fmt.Printf("Received data: %s \n", m.Data);
        fmt.Printf("Received type: %s \n", m.MType);
        
        // sending ack
        encoder := gob.NewEncoder(conn)
        var p Message;
        p.Number = m.Number;
        p.Data = "";
        p.MType = ACK_MSG;
        encoder.Encode(p)
    }
	conn.Close()
}